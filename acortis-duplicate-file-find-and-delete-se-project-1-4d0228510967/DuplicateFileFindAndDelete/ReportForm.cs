﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DuplicateFileFindAndDelete
{
    public partial class ReportForm : Form
    {
        public ReportForm(string report)
        {
            InitializeComponent();


            webBrowser1.DocumentText +=
                "<!-- Begin: adBrite, Generated: 2012-05-16 17:04:05  -->" +
                "<script type=\"text/javascript\" src=\"http://ads.adbrite.com/mb/text_group.php?sid=2152296&br=1\"></script>" +
                "<!-- End: adBrite -->" +
                report.Replace("\n", "<br/>").Replace("Total files to search", "<!-- Begin: adBrite, Generated: 2012-05-16 16:49:09  -->" +
                "<script type=\"text/javascript\">" +
                "var AdBrite_Title_Color = '0000FF';" +
                "var AdBrite_Text_Color = '000000';" +
                "var AdBrite_Background_Color = 'FFFFFF';" +
                "var AdBrite_Border_Color = 'CCCCCC';" +
                "var AdBrite_URL_Color = '008000';" +
                "try{var AdBrite_Iframe=window.top!=window.self?2:1;var AdBrite_Referrer=document.referrer==''?document.location:document.referrer;AdBrite_Referrer=encodeURIComponent(AdBrite_Referrer);}catch(e){var AdBrite_Iframe='';var AdBrite_Referrer='';}" +
                "</script>" +
                "<script type=\"text/javascript\">document.write(String.fromCharCode(60,83,67,82,73,80,84));document.write(' src=\"http://ads.adbrite.com/mb/text_group.php?sid=2152288&zs=3330305f323530&ifr='+AdBrite_Iframe+'&ref='+AdBrite_Referrer+'\" target=\"_blank\" type=\"text/javascript\">');document.write(String.fromCharCode(60,47,83,67,82,73,80,84,62));</script>" +
                "<div><a target=\"_blank\" href=\"http://www.adbrite.com/mb/commerce/purchase_form.php?opid=2152288&afsid=1\" style=\"font-weight:bold;font-family:Arial;font-size:13px;\">Your Ad Here</a></div>" +
                "<!-- End: adBrite --><br>Total files to search");

            Timer t = new Timer();
            t.Interval = 24000;

            t.Tick += new EventHandler(t_Tick);
            t.Enabled = true;
            t.Start();
        }

        void t_Tick(object sender, EventArgs e)
        {
            webBrowser1.Refresh(WebBrowserRefreshOption.Completely);
        }

        private void ReportForm_Load(object sender, EventArgs e)
        {
        }
    }
}
