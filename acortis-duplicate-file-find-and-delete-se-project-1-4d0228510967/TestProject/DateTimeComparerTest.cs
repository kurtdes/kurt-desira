﻿using FileFindAndDeleteLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for DateTimeComparerTest and is intended
    ///to contain all DateTimeComparerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DateTimeComparerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Compare
        ///</summary>
        [TestMethod()]
        public void CompareTest()
        {
            DateTimeComparer target = new DateTimeComparer(); // TODO: Initialize to an appropriate value
            DateTime x = DateTime.Now; // TODO: Initialize to an appropriate value
            DateTime y = DateTime.Now; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }


        [TestMethod()]
        public void CompareTest1()
        {
            DateTimeComparer target = new DateTimeComparer(); // TODO: Initialize to an appropriate value
            DateTime x = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime y = DateTime.Now; // TODO: Initialize to an appropriate value
            int expected = -1; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }


        [TestMethod()]
        public void CompareTest2()
        {
            DateTimeComparer target = new DateTimeComparer(); // TODO: Initialize to an appropriate value
            DateTime x = new DateTime(00,00,00); // TODO: Initialize to an appropriate value
            DateTime y = DateTime.Now; // TODO: Initialize to an appropriate value
            int expected = -1; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }


        [TestMethod()]
        public void CompareTest3()
        {
            DateTimeComparer target = new DateTimeComparer(); // TODO: Initialize to an appropriate value
            DateTime x = new DateTime(2014, 01, 12); // TODO: Initialize to an appropriate value
            DateTime y = DateTime.Now; // TODO: Initialize to an appropriate value
            int expected = 1; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }






        [TestMethod()]
        public void CompareTest4()
        {
            DateTimeComparer target = new DateTimeComparer(); // TODO: Initialize to an appropriate value
            DateTime x = DateTime.Now; // TODO: Initialize to an appropriate value
            DateTime y = DateTime.Now; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }


        [TestMethod()]
        public void CompareTest5()
        {
            DateTimeComparer target = new DateTimeComparer(); // TODO: Initialize to an appropriate value
            DateTime x = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime y = new DateTime(); // TODO: Initialize to an appropriate value
            int expected = -1; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }


        [TestMethod()]
        public void CompareTest6()
        {
            DateTimeComparer target = new DateTimeComparer(); // TODO: Initialize to an appropriate value
            DateTime x = new DateTime(00, 00, 00); // TODO: Initialize to an appropriate value
            DateTime y = new DateTime(00, 00, 00); // TODO: Initialize to an appropriate value
            int expected = -1; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }


        [TestMethod()]
        public void CompareTest7()
        {
            DateTimeComparer target = new DateTimeComparer(); // TODO: Initialize to an appropriate value
            DateTime x = new DateTime(2014, 01, 12); // TODO: Initialize to an appropriate value
            DateTime y = new DateTime(2014, 01, 12); // TODO: Initialize to an appropriate value
            int expected = 1; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Compare(x, y);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }




    }
}
