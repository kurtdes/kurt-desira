﻿using FileFindAndDeleteLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Collections.Generic;

namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for FindAndDeleteTest and is intended
    ///to contain all FindAndDeleteTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FindAndDeleteTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetFilesFromDirectory
        ///</summary>
        [TestMethod()]
        [DeploymentItem("FileFindAndDeleteLibrary.dll")]
        public void GetFilesFromDirectoryTest1()
        {
            string folder = "C:\\Users\\Kurt\\Desktop\\abc"; // TODO: Initialize to an appropriate value
            string[] filters = new String[1]; // TODO: Initialize to an appropriate value

            filters[0] = "*.txt";
            int expected = 1;
            int actual = 0;

            IEnumerable<FileInfo> returns = FindAndDelete.GetFilesFromDirectory(folder, filters);
           // IEnumerable<FileInfo> actual;


            foreach (FileInfo info in returns)
            {
                actual++;
            }
            Assert.AreEqual(expected, actual);
          //  Assert.Inconclusive("Verify the correctness of this test method.");
        }




        [TestMethod()]
        [DeploymentItem("FileFindAndDeleteLibrary.dll")]
        public void GetFilesFromDirectoryTest()
        {
            string folder = "C:\\Users\\Kurt\\Desktop\\xyz"; // TODO: Initialize to an appropriate value
            string[] filters = new String[1]; // TODO: Initialize to an appropriate value

            filters[0] = "*.txt";
            int expected = 1;
            int actual = 0;

            IEnumerable<FileInfo> returns = FindAndDelete.GetFilesFromDirectory(folder, filters);
            // IEnumerable<FileInfo> actual;


            foreach (FileInfo info in returns)
            {
                actual++;
            }
            Assert.AreEqual(expected, actual);
            //  Assert.Inconclusive("Verify the correctness of this test method.");
        }




        [TestMethod()]
        [DeploymentItem("FileFindAndDeleteLibrary.dll")]
        public void GetFilesFromDirectoryTest2()
        {
            string folder = null; //"C:\\Users\\Kurt\\Desktop\\abc"; // TODO: Initialize to an appropriate value
            string[] filters = new String[1]; // TODO: Initialize to an appropriate value

            filters[0] = "*.txt";
            int expected = 1;
            int actual = 0;

            IEnumerable<FileInfo> returns = FindAndDelete.GetFilesFromDirectory(folder, filters);
            // IEnumerable<FileInfo> actual;


            foreach (FileInfo info in returns)
            {
                actual++;
            }
            Assert.AreEqual(expected, actual);
            //  Assert.Inconclusive("Verify the correctness of this test method.");
        }



        [TestMethod()]
        [DeploymentItem("FileFindAndDeleteLibrary.dll")]
        public void GetFilesFromDirectoryTest3()
        {
            string folder = "C:\\Users\\Kurt\\Desktop\\abc"; // TODO: Initialize to an appropriate value
            string[] filters = new String[1]; // TODO: Initialize to an appropriate value

            filters[0] = "*.txt";
            int expected = 1;
            int actual = 0;

            IEnumerable<FileInfo> returns = FindAndDelete.GetFilesFromDirectory(folder, filters);
            // IEnumerable<FileInfo> actual;


            foreach (FileInfo info in returns)
            {
                actual++;
            }
            Assert.AreEqual(expected, actual);
            //  Assert.Inconclusive("Verify the correctness of this test method.");
        }


        [TestMethod()]
        //  [DeploymentItem("FileFindAndDeleteLibrary.dll")]
        public void GetFilesFromDirectoryTest4()
        {
            string folder = "C:\\Users\\Kurt\\Desktop\\abc"; // TODO: Initialize to an appropriate value
            string[] filters = new string[2]; // TODO: Initialize to an appropriate value

            filters[0] = "*.txt";
            filters[1] = "*.jpg";
            int expected = 2;
            int actual = 0;

            IEnumerable<FileInfo> returns = FindAndDelete.GetFilesFromDirectory(folder, filters);
            // IEnumerable<FileInfo> actual;


            foreach (FileInfo info in returns)
            {
                actual++;
            }
            Assert.AreEqual(expected, actual);
            //  Assert.Inconclusive("Verify the correctness of this test method.");
        }


        [TestMethod()]
        //  [DeploymentItem("FileFindAndDeleteLibrary.dll")]
        public void GetFilesFromDirectoryTest5()
        {
            string folder = "C:\\Users\\Kurt\\Desktop\\abc"; // TODO: Initialize to an appropriate value
            string[] filters = new string[2]; // TODO: Initialize to an appropriate value

            filters[0] = "*.txt";
            filters[1] = "*.hhh";
            int expected = 2;
            int actual = 0;

            IEnumerable<FileInfo> returns = FindAndDelete.GetFilesFromDirectory(folder, filters);
            // IEnumerable<FileInfo> actual;


            foreach (FileInfo info in returns)
            {
                actual++;
            }
            Assert.AreEqual(expected, actual);
            //  Assert.Inconclusive("Verify the correctness of this test method.");
        }



        [TestMethod()]
        //  [DeploymentItem("FileFindAndDeleteLibrary.dll")]
        public void GetFilesFromDirectoryTest6()
        {
            string folder = "C:\\Users\\Kurt\\Desktop\\abc"; // TODO: Initialize to an appropriate value
            string[] filters = new string[2]; // TODO: Initialize to an appropriate value

            filters[0] = "*.txt";
            filters[1] = "*.hhh";
            int expected = 1;
            int actual = 0;

            IEnumerable<FileInfo> returns = FindAndDelete.GetFilesFromDirectory(folder, filters);
            // IEnumerable<FileInfo> actual;


            foreach (FileInfo info in returns)
            {
                actual++;
            }
            Assert.AreEqual(expected, actual);
            //  Assert.Inconclusive("Verify the correctness of this test method.");
        }

    }
}
