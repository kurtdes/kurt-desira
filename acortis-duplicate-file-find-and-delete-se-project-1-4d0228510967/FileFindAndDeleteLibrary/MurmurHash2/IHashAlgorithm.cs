﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileFindAndDeleteLibrary.MurmurHash2
{
    public interface IHashAlgorithm
    {
        UInt32 Hash(Byte[] data);
    }

    public interface ISeededHashAlgorithm : IHashAlgorithm
    {
        UInt32 Hash(Byte[] data, UInt32 seed);

    }
}
