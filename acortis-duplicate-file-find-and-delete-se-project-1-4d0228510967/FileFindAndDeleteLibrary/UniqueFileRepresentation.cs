﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace FileFindAndDeleteLibrary
{
    internal class DateTimeComparer : IComparer<DateTime>
    {
        #region IComparer<DateTime> Members

        public int Compare(DateTime x, DateTime y)
        {
            int result = x.Ticks.CompareTo(y.Ticks);

            if (result != 0)
            {
                return result;
            }
            else
            {
                return Guid.NewGuid().CompareTo(Guid.NewGuid());
            }
        }

        #endregion
    }

    /// <summary>
    /// Represents a List of Duplicate Files
    /// </summary>
    class UniqueFileRepresentation
    {
        Guid id = Guid.NewGuid();

        Guid Id { get { return id; } }

        SortedList<DateTime, FileInfo> sortedDuplicateFileList = new SortedList<DateTime, FileInfo>(new DateTimeComparer());

        public SortedList<DateTime, FileInfo> SortedDuplicateFileList
        {
            get { return sortedDuplicateFileList; }
        }

        Dictionary<string, FileInfo> filesRepesented = new Dictionary<string, FileInfo>();

        public Dictionary<string, FileInfo> FilesRepesented
        {
            get { return filesRepesented; }
        }

        public void AddFile(FileInfo fileInfo)
        {
            string path = fileInfo.DirectoryName + fileInfo.Name;

            if (filesRepesented.ContainsKey(path))
            {
                throw new ArgumentException("This file was already added!");
            }

            filesRepesented.Add(path, fileInfo);
            sortedDuplicateFileList.Add(fileInfo.LastWriteTimeUtc, fileInfo);
        }
    }
}
